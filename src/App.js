import React, { Component } from 'react';


import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AllBoards from './components/boardFetching';
import ListOfBoard from './components/listFetching';

class App extends Component {
  state = {};
  render() {
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route path="/" exact component={AllBoards} />
            <Route path="/board/:id" component={ListOfBoard} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
