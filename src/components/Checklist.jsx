import React, { Component } from 'react';
import Checkitem from './Checkitem';
import Form from "./form";
class Checklist extends Component {
  state = {
    inputForChecklistitems: []
  };


  render() {
    return (
      <div>
        <div className="checklistHeader">
          <h3>{this.props.checklistDetails.name}</h3>
          <button
            className="btn btn-close"
            onClick={() => {
              this.props.handleDeleteCheckList(this.props.checklistDetails.id);
            }}
          >
            X
          </button>
        </div>
        {this.props.checklistDetails.checkItems.map(checkItem => (
          <Checkitem
            key={checkItem.id}
            checkItemDetails={checkItem}
            handleUpdateCheckItem={this.props.handleUpdateCheckItem}
            handleDeleteCheckItem={this.props.handleDeleteCheckItem}
            checklistDetails={this.props.checklistDetails}
          />
        ))}
     <Form 
            formName = "Add CheckItems"
            handleAddCheckItem = {this.props.handleAddCheckItem}
            checkList = {this.props.checklistDetails} />
      </div>
    );
  }
}

export default Checklist;
