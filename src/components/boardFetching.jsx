import React from 'react';

import { Link } from 'react-router-dom';


class AllBoards extends React.PureComponent {
  constructor() {
    super();
    this.state = { data: [] };
  }
  componentDidMount() {
    const url = `https://api.trello.com/1/members/me/boards?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
    fetch(url, {
      method: 'Get'
    })
      .then(res => res.json())
      .then(json => this.setState({ data: json }));
  }


render() {
  if (this.state.data.length === 0) {
    return <h1>Loading...</h1>;
  }
  return (
    <div className = "Boards">
        {this.state.data.map(el => (
          <h1 key = {el.id}>
          <Link to = {`/board/${el.id}`}> {el.name} </Link>
          </h1>
        ))}
    </div>
  );
}
}

export default AllBoards;
