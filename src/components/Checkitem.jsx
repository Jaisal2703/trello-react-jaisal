import React, { Component } from 'react';

class Checkitem extends Component {
  state = {
    checkState: true
  };

  //Function will update the state according to check and uncheck activity

  handleCheckState = state => {
    this.setState({ checkState: state });
  };
  componentDidMount() {
    if (this.props.checkItemDetails.state === 'incomplete') {
      this.setState({ checkState: false });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="chechklistitems">
          <div className="textItem">
            <input
              type="checkbox"
              checked={this.state.checkState}
              onChange={event => {
                this.props.handleUpdateCheckItem(
                  this.props.checkItemDetails,
                  event,
                  this.props.checklistDetails.idCard
                );
                this.handleCheckState(event.target.checked);
              }}
            />{' '}
            {this.props.checkItemDetails.name}
          </div>
          <button
            className="btn btn-close"
            onClick={() => {
              this.props.handleDeleteCheckItem(this.props.checkItemDetails);
            }}
          >
            X
          </button>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default Checkitem;
