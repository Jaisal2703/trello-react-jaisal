import React, { Component } from 'react';
import Form from "./form"

class CardOfList extends Component {
  state = {
    cardsDetails: [],
    inputForCard: ' ',
    open: false
  };


  componentDidMount() {
    const url = `https://api.trello.com/1/lists/${this.props.listDetails.id}/cards?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
    fetch(url, {
      method: 'Get'
    })
      .then(res => res.json())
      .then(json => this.setState({ cardsDetails: json }));
  }

  // Function will add a new card 
  handleAddcard = cardName => {
    fetch(
      `https://api.trello.com/1/cards?name=${cardName}&idList=${this.props.listDetails.id}&keepFromSource=all&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      { method: 'POST' }
    )
      .then(response => response.json())
      .then(element => {
        this.setState({
          cardsDetails: this.state.cardsDetails.concat([element])
        });
      });
  };
  // Function will delete a card once a delete operation is performed
  
  handleDeleteCard = deleteCard => {
    this.setState({
      cardsDetails: this.state.cardsDetails.filter(
        card => card.id !== deleteCard
      )
    });
    fetch(
      `https://api.trello.com/1/cards/${deleteCard}?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      { method: 'DELETE' }
    );
  };
  render() {

    return (
      <div className="cards">
        <div className="card-body">
          {this.state.cardsDetails.map(eachCard => (
            <div className="card">
            <button
              type="button"
              className="btn btn-card"
              onClick={() => {
                this.props.onOpenModal(eachCard);
              }}
            >
              {eachCard.name}
            </button>
            <button
              className="btn btn-close"
              onClick={() => {
                this.handleDeleteCard(eachCard.id);
              }}
            >
              X
            </button>
          </div>
          ))}
        </div>
        <Form 
        formName = "Add Card"
        handleAddcard = {this.handleAddcard} />
      </div>
    );
  }
}

export default CardOfList;
