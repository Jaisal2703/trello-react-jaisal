import React from 'react';
import Form from './form';
import ModalComponent from './modalForChecklist';
import CardOfList from './cardFetching';

class ListOfBoard extends React.Component {
  state = {
    listDetails: [],
    checklistsDetails: [],
    open: false,
    inputForList: []
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?cards=none&card_fields=all&filter=open&fields=all&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`
    )
      .then(response => response.json())
      .then(lists => this.setState({ listDetails: lists }));
  }

  // Function will add a new list
  handleAddList = listName => {
    if (listName !== '') {
      fetch(
        `https://api.trello.com/1/lists?name=${listName}&idBoard=${this.props.match.params.id}&pos=bottom&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
        {
          method: 'POST'
        }
      )
        .then(response => response.json())
        .then(list => {
          this.setState({ listDetails: this.state.listDetails.concat([list]) });
        });
    }
  };
  // Function will add a new checklist

  handleAddChecklist = checkListName => {
    fetch(
      `https://api.trello.com/1/cards/${this.state.cardId}/checklists?name=${checkListName}&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      { method: 'POST' }
    )
      .then(response => response.json())
      .then(checkList =>
        this.setState({
          checklistsDetails: this.state.checklistsDetails.concat([checkList])
        })
      );
  };
  // Function will add a new checkitem

  handleAddCheckItem = (checkItem, checklist) => {
    fetch(
      `https://api.trello.com/1/checklists/${checklist.id}/checkItems?name=${checkItem}&pos=bottom&checked=false&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      {
        method: 'POST'
      }
    )
      .then(response => response.json())
      .then(data => {
        const checklistsDetails = this.state.checklistsDetails;

        checklistsDetails[
          checklistsDetails.indexOf(checklist)
        ].checkItems = checklistsDetails[
          checklistsDetails.indexOf(checklist)
        ].checkItems.concat([data]);
        this.setState({
          checklistsDetails: checklistsDetails
        });
      });
  };
  // Function will delete the selected checkitem

  handleDeleteCheckItem = checkItemDetails => {
    fetch(
      `https://api.trello.com/1/checklists/${checkItemDetails.idChecklist}/checkItems/${checkItemDetails.id}?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      { method: 'DELETE' }
    );

    this.setState({
      checklistsDetails: this.state.checklistsDetails.map(checkList => {
        if (checkList.id === checkItemDetails.idChecklist) {
          checkList.checkItems = checkList.checkItems.filter(
            c => c.id !== checkItemDetails.id
          );
        }

        return checkList;
      })
    });
  };
  // Function will update the state of a checkitem

  handleUpdateCheckItem = (checkItem, event, idCard) => {
    let state;
    if (event.target.checked === true) {
      state = 'complete';
    } else {
      state = 'incomplete';
    }
    fetch(
      `https://api.trello.com/1/cards/${idCard}/checkItem/${checkItem.id}?state=${state}&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      { method: 'PUT' }
    ).then(response => {
      response.json().then(data => {
        this.setState({
          checklistsDetails: this.state.checklistsDetails.map(checkList => {
            if (checkList.id === checkItem.idChecklist) {
              checkList.checkItems = checkList.checkItems.map(c => {
                if (c.id === checkItem.id) {
                  c.state = state;
                }
                return c;
              });
            }

            return checkList;
          })
        });
      });
    });
  };
  //Function will open a modal for once user click on card
  onOpenModal = cardDetails => {
    this.setState({ checklistsDetails: [] });
    fetch(
      `https://api.trello.com/1/cards/${cardDetails.id}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`
    )
      .then(response => response.json())
      .then(checkLists => this.setState({ checklistsDetails: checkLists }));
    this.setState({ cardId: cardDetails.id, open: true });
  };
  handleDeleteCheckList = idCheckList => {
    fetch(
      `https://api.trello.com/1/checklists/${idCheckList}?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
      { method: 'DELETE' }
    );
    this.setState({
      checklistsDetails: this.state.checklistsDetails.filter(
        checklist => checklist.id !== idCheckList
      )
    });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const mystyle = {
      display: 'flex',
      fontSize: '2em',
      flexDirection: 'column',
      padding: '0.5em',
      borderRadius: '0.2em'
    };
    return (
      <div className="Lists">
        {this.state.listDetails.map(element => (
          <div style={mystyle} className="List" key={element.id}>
            {element.name}
            <CardOfList
              key={element.id}
              listDetails={element}
              onOpenModal={this.onOpenModal}
            />
          </div>
        ))}
        <Form formName="Add List" handleAddList={this.handleAddList} />

        <ModalComponent
          open={this.state.open}
          closeModal={this.onCloseModal}
          checklistsDetails={this.state.checklistsDetails}
          handleAddChecklist={this.handleAddChecklist}
          handleAddCheckItem={this.handleAddCheckItem}
          handleDeleteCheckItem={this.handleDeleteCheckItem}
          handleUpdateCheckItem={this.handleUpdateCheckItem}
          handleDeleteCheckList={this.handleDeleteCheckList}
        />
      </div>
    );
  }
}

export default ListOfBoard;
